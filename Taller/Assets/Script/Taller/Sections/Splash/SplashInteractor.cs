﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface SplashInteractorInput
{
	void getConfig();
}

public interface SplashInteractorOutPut {
	void configurationDidFinish();
}


public class SplashInteractor: SplashInteractorInput {
    
    public delegate void WhateverType(WWW www);
	public SplashInteractorOutPut outPut;
    private ConfigurationServices services;

    //- Constructor -

	public SplashInteractor() {
		Debug.Log("SPLASH INTERACTOR INIT");
		GameObject gameObject = new GameObject();
		services = gameObject.AddComponent(typeof(ConfigurationServices)) as ConfigurationServices;
	}
    

	//- SplashInteractorInput -

	public void getConfig() {
		Debug.Log("SPLASH INTERACTOR getConfig");
        services.configurationService(ConfigurationServiceDidComplete);
	}


	//- ConfigurationServicesOutPut -

	void ConfigurationServiceDidComplete(WWW www)
	{
		Debug.Log("SPLASH INTERACTOR ConfigurationServiceDidComplete");
		outPut.configurationDidFinish();
    }
}
