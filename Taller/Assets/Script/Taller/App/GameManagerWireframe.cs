﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public interface GameManagerWireframeInput
{
	void showSplash();
	void showHome();
}

public interface GameManagerWireframeOutPut {
	void splashDidFinish();
}


public class GameManagerWireframe: GameManagerWireframeInput {

	private GameManager gameManager;
	private SplashVC splashVC;
	private HomeVC homeVC;
	public GameManagerWireframeOutPut outPut;
    

	public GameManagerWireframe(GameManager gameManagerInput)
    {
		gameManager = gameManagerInput;
    }

	// GameManagerWireframeInput

	public void showSplash () {
		splashVC = gameManager.gameObject.AddComponent(typeof(SplashVC)) as SplashVC;
		SplashPresenter presenter = new SplashPresenter();
		presenter.wireframe = this;
		splashVC.presenter = presenter;
	}

	public void showHome()
	{
        Debug.Log("WIREFRAME HOME");      
	}

    // Method Finish

	public void splashDidFinish() {
		outPut.splashDidFinish();
	}
}
