﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour, GameManagerWireframeOutPut {
    
	private GameManagerWireframe wireframe;

	void Start () {
		wireframe = new GameManagerWireframe(this);
		wireframe.outPut = this;
		wireframe.showSplash();
	}
	
	void Update () {
		
	}

	//- GameManagerWireframeOutPut -

	public void splashDidFinish() {
        Debug.Log("GAME MANAGER PREPARE HOME");
		wireframe.showHome();
	}   
}
