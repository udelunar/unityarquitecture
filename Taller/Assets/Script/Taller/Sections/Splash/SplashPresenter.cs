﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface SplashPresenterUI {
	void viewDidLoad();
}


public class SplashPresenter : SplashPresenterUI, SplashInteractorOutPut
{

    private SplashInteractor interactor;
	public GameManagerWireframe wireframe;
    

	public SplashPresenter() {
		interactor = new SplashInteractor();
		interactor.outPut = this;
	}

    // INPUT METHOD

	public void viewDidLoad() {
        Debug.Log("START SPLASH PRESENTER");
		interactor.getConfig();
	}


	// SplashInteractorOutPut

	public void configurationDidFinish() {
		Debug.Log("START SPLASH PRESENTER!!! configurationDidFinish");
		wireframe.splashDidFinish();
	}
}
