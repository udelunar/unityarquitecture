﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;
using System.IO;


public interface ConfigurationServicesInput
{
	void configurationService(SplashInteractor.WhateverType myCallback);
}


public class ConfigurationServices : MonoBehaviour {
	
	protected SplashInteractor.WhateverType callbackFct;

	public void configurationService(SplashInteractor.WhateverType myCallback)
    {
        callbackFct = myCallback;

        WWW www = new WWW(Constants.urlConfiguration);

        StartCoroutine(GetJsonConfiguration(www));
    }


    IEnumerator GetJsonConfiguration(WWW www)
    {
        yield return www;

        if (www.error != null)
        {
			Debug.Log("----------->>>> Error while Downloading Data: " + www.error);
        }
        else
        {
            callbackFct(www);
        }
    }
}
